from pygame.locals import *

import pygame


class Game:
    def __init__(self):
        WIDTH, HEIGHT = 700, 1000
        pygame.init()
        self.surface = pygame.display.set_mode((WIDTH, HEIGHT))
        self.player_input = {}
        self.running = True
    
    def run(self):
        while self.running:
            self.update()
            self.draw()

    def update(self):
        self.player_input = self.get_player_input()
        if self.player_input["quit"]:
            self.running = False

    def draw(self):
        self.surface.fill("gray10")
        pygame.display.flip()

    @staticmethod
    def get_player_input():
        player_input = {
            "quit": False,
        }

        for event in pygame.event.get():
            if event.type == QUIT:
                player_input["quit"] = True
            
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    player_input["quit"] = True
        
        return player_input
